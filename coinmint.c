#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#include "densities.h"
#include "sampler.h"
#include "sampler_utils.h"

double* data = NULL;
int ndata = 0;

double data_likelihood(double* x, int id) {
	double sum = 0.0;
	for (int j=0; j<ndata; j++) {
		int cid = data[j*2];
		if (cid == id) {
			int flip = data[j*2+1];
			sum += dbernoulli(flip, x[cid]);	
		}
	}
	return sum;
}

double hyper_likelihood(double* x) {
	double sum = beta_likelihood(x, 15, x[15], x[16]);
	return sum;
}

double pCoin_0(double* x) {
	return dbeta(x[0], x[15], x[16]) + data_likelihood(x, 0);	
}
double pCoin_1(double* x) {
	return dbeta(x[1], x[15], x[16]) + data_likelihood(x, 1);	
}
double pCoin_2(double* x) {
	return dbeta(x[2], x[15], x[16]) + data_likelihood(x, 2);	
}
double pCoin_3(double* x) {
	return dbeta(x[3], x[15], x[16]) + data_likelihood(x, 3);	
}
double pCoin_4(double* x) {
	return dbeta(x[4], x[15], x[16]) + data_likelihood(x, 4);	
}
double pCoin_5(double* x) {
	return dbeta(x[5], x[15], x[16]) + data_likelihood(x, 5);	
}
double pCoin_6(double* x) {
	return dbeta(x[6], x[15], x[16]) + data_likelihood(x, 6);	
}
double pCoin_7(double* x) {
	return dbeta(x[7], x[15], x[16]) + data_likelihood(x, 7);	
}
double pCoin_8(double* x) {
	return dbeta(x[8], x[15], x[16]) + data_likelihood(x, 8);	
}
double pCoin_9(double* x) {
	return dbeta(x[9], x[15], x[16]) + data_likelihood(x, 9);	
}
double pCoin_10(double* x) {
	return dbeta(x[10], x[15], x[16]) + data_likelihood(x, 10);	
}
double pCoin_11(double* x) {
	return dbeta(x[11], x[15], x[16]) + data_likelihood(x, 11);	
}
double pCoin_12(double* x) {
	return dbeta(x[12], x[15], x[16]) + data_likelihood(x, 12);	
}
double pCoin_13(double* x) {
	return dbeta(x[13], x[15], x[16]) + data_likelihood(x, 13);	
}
double pCoin_14(double* x) {
	return dbeta(x[14], x[15], x[16]) + data_likelihood(x, 14);	
}

double pAlpha(double* x) {
	return duniform(x[15], 0, 25) + hyper_likelihood(x);	
}

double pBeta(double* x) {
	return duniform(x[16], 0, 25) + hyper_likelihood(x);	
}

int main(int argc, char* argv[]) {
	srand(time(NULL));

	if (argc < 3) {
		printf("USAGE: %s <input file> <nsamples>\n", argv[0]);
		return 0;
	}
	
	read_file(argv[1], &data, &ndata);
	int nsample = atoi(argv[2]);	

	density f[] = { 
		pCoin_0, pCoin_1, pCoin_2, pCoin_3,
		pCoin_4, pCoin_5, pCoin_6, pCoin_7,
		pCoin_8, pCoin_9, pCoin_10, pCoin_11,
		pCoin_12, pCoin_13, pCoin_14,
		pAlpha, pBeta 
	};

	double* x = (double*)malloc(sizeof(double) * 17);

	for (int j=0; j<17; j++) {
		x[j] = rand() / (double)RAND_MAX;
	}

	sampler(nsample, f, x, 17, print_sample);

	free(x);
	free(data);

	return 0;
}
