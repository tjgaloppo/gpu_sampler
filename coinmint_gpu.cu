#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#define _USE_MATH_DEFINES

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#include "densities.h"
#include "sampler.h"
#include "sampler_utils.h"

double* data = NULL;
double* dev_data = NULL;
double* host_result = NULL;
double* dev_result = NULL;
int ndata = 0;
int nblock = 0;
int nthread = 256;
int shm_sz = 0;

__device__ void sum_array(double* x, int n, int tid) {
	do {
		int offset = n / 2;
		if (tid + offset < n) {
			x[tid] += x[tid + offset];
		}
		n = offset;
		__syncthreads();
	} while (n > 1);
}

__global__ void compute_likelihood(double* result, double* u, int m, int id, double p) {
	extern __shared__ double cache[];

	int bid = blockIdx.x;
	int tid = threadIdx.x;
	int uidx = bid * blockDim.x + tid;

	cache[tid] = 0.0;
	if (uidx < m) {
		int cid = (int)u[2*uidx];
		if (cid == id) {
			cache[tid] = dbernoulli(u[2 * uidx + 1], p);
		}
	}

	__syncthreads();

	sum_array(cache, blockDim.x, tid);

	if (tid == 0) {
		result[bid] = cache[0];
	}
}

double data_likelihood_cpu(double* x, int id) {
	double sum = dbeta(x[id], x[15], x[16]);
	for (int j = 0; j<ndata; j++) {
		int cid = data[j * 2];
		if (cid == id) {
			int flip = data[j * 2 + 1];
			sum += dbernoulli(flip, x[cid]);
		}
	}
	return sum;
}

double data_likelihood_gpu(double* x, int id) {
	compute_likelihood<<<nblock, nthread, shm_sz>>>(dev_result, dev_data, ndata, id, x[id]);

	double result = dbeta(x[id], x[15], x[16]);
	
	cudaDeviceSynchronize();

	for (int j = 0; j < nblock; j++) {
		result += host_result[j];
	}

//	double hresult = data_likelihood_cpu(x, id);
//	printf("%.6f %.6f\n", result, hresult);

	return result;
}

double (*data_likelihood)(double*, int) = data_likelihood_gpu;

double hyper_likelihood(double* x) {
	return beta_likelihood(x, 15, x[15], x[16]);
}

double pCoin_0(double* x) {
	return data_likelihood(x, 0);
}
double pCoin_1(double* x) {
	return data_likelihood(x, 1);
}
double pCoin_2(double* x) {
	return data_likelihood(x, 2);
}
double pCoin_3(double* x) {
	return data_likelihood(x, 3);
}
double pCoin_4(double* x) {
	return data_likelihood(x, 4);
}
double pCoin_5(double* x) {
	return data_likelihood(x, 5);
}
double pCoin_6(double* x) {
	return data_likelihood(x, 6);
}
double pCoin_7(double* x) {
	return data_likelihood(x, 7);
}
double pCoin_8(double* x) {
	return data_likelihood(x, 8);
}
double pCoin_9(double* x) {
	return data_likelihood(x, 9);
}
double pCoin_10(double* x) {
	return data_likelihood(x, 10);
}
double pCoin_11(double* x) {
	return data_likelihood(x, 11);
}
double pCoin_12(double* x) {
	return data_likelihood(x, 12);
}
double pCoin_13(double* x) {
	return data_likelihood(x, 13);
}
double pCoin_14(double* x) {
	return data_likelihood(x, 14);
}

double pAlpha(double* x) {
	return duniform(x[15], 0, 25) + hyper_likelihood(x);
}

double pBeta(double* x) {
	return duniform(x[16], 0, 25) + hyper_likelihood(x);
}

int main(int argc, char* argv[]) {
	srand((unsigned int)time(NULL));

	if (argc < 3) {
		printf("USAGE: %s <input file> <nsamples>\n", argv[0]);
		return 0;
	}

	if (argc == 4 && !strcmp(argv[3], "cpu")) {
		data_likelihood = data_likelihood_cpu;
	}

	read_file(argv[1], &data, &ndata);
	int nsample = atoi(argv[2]);

	nblock = (ndata + nthread - 1) / nthread;
	shm_sz = sizeof(double) * nthread;

	cudaSetDevice(0);

	cudaMalloc(&dev_data, sizeof(double) * 2 * ndata);
	cudaMemcpy(dev_data, data, sizeof(double) * 2 * ndata, cudaMemcpyHostToDevice);

	cudaHostAlloc(&host_result, sizeof(double) * nblock, cudaHostAllocMapped);
	cudaHostGetDevicePointer(&dev_result, host_result, 0);

	density f[] = {
		pCoin_0, pCoin_1, pCoin_2, pCoin_3,
		pCoin_4, pCoin_5, pCoin_6, pCoin_7,
		pCoin_8, pCoin_9, pCoin_10, pCoin_11,
		pCoin_12, pCoin_13, pCoin_14,
		pAlpha, pBeta
	};

	double *x = (double*)malloc(sizeof(double) * 17);

	for (int j = 0; j<17; j++) {
		x[j] = rand() / (double)RAND_MAX;
	}

	sampler(nsample, f, x, 17, print_sample);

	free(x);

	cudaFreeHost(host_result);
	cudaFree(dev_data);
	cudaDeviceReset();

	free(data);

	return 0;
}
