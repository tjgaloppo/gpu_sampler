#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void print_sample(double* x, int k) {
		printf("%f", x[0]);
		for (int j=1; j<k; j++) {
			printf(",%f", x[j]);
		}
		printf("\n");
}

void read_file(char* filename, double** ptr, int* nrow) {
	FILE* pfile = fopen(filename, "rt");
	char buf[1025];

	int nalloc = 0;
	int cpos = 0;

	*ptr = NULL;
	*nrow = 0;
	
	memset(buf, 0, 1025);
	
	if (pfile) {
		while ( fgets(buf, 1024, pfile) ) {
			*strchr(buf, '\n') = 0;
			
			char* value = strtok(buf, ",");
			while (NULL != value) {
				double u = atof(value);
				
				cpos += 1;
				if (cpos > nalloc) {
					nalloc += 256;
					*ptr = (double*)realloc(*ptr, sizeof(double) * nalloc);
				}
				(*ptr)[cpos-1] = u;
				
				value = strtok(NULL, ",");
			}
			
			*nrow += 1;
		}
		
		fclose(pfile);
	}
}
