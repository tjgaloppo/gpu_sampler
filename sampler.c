#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "sampler.h"

void sampler(int n, density* f, double* x, int k, sample_callback cb) {
	for (int i=0; i<n; i++) {
		for (int j=0; j<k; j++) {
			double p = f[j](x);
			if (p != p) p = -INFINITY;
			double tmp = x[j];
			x[j] += rand() / (double)RAND_MAX - 0.5;
			double pc = f[j](x);
			if (pc != pc) pc = -INFINITY;
			if (pc < p) {
				double ratio = exp(pc - p);
				double rnum = rand() / (double)RAND_MAX;
				if (ratio < rnum) {
					x[j] = tmp;
				}	
			}
		}	

		if (NULL != cb) {
			cb(x, k);
		}
	}
}
