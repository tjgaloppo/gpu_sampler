#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#include "densities.h"
#include "sampler.h"
#include "sampler_utils.h"

double* data = NULL;
int ndata = 0;

double pMean(double* x) {
	return dgaussian(x[0], 5, 2) + gaussian_likelihood(data, ndata, x[0], x[1]);	
}

double pSigma(double* x) {
	return dgamma(x[1], 3, 3) + gaussian_likelihood(data, ndata, x[0], x[1]);	
}

int main(int argc, char* argv[]) {
	if (argc < 3) {
		printf("USAGE: %s <input file> <nsamples>\n", argv[0]);
		return 0;
	}
	
	read_file(argv[1], &data, &ndata);
	int nsample = atoi(argv[2]);	

	srand(time(NULL));

	density f[] = { pMean, pSigma };
	double x[] = { 0, 0 };
	sampler(nsample, f, x, 2, print_sample);

	free(data);
	
	return 0;
}
