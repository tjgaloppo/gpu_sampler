#ifndef _SAMPLER_H_
#define _SAMPLER_H_

typedef double (*density)(double*);
typedef void (*sample_callback)(double*, int);

void sampler(int, density*, double*, int, sample_callback);

#endif
