#ifndef _DENSITIES_H_
#define _DENSITIES_H_

#include "cuda_runtime.h"

/* log-density functions in both cpu and gpu space */
__host__ __device__ double dbernoulli(int, double);
__host__ __device__ double duniform(double, double, double);
__host__ __device__ double dgaussian(double, double, double);
__host__ __device__ double dbeta(double, double, double);
__host__ __device__ double dgamma(double, double, double);
__host__ __device__ double dbinomial(int, int, double);

/* stock data log-likelihood functions in cpu space only */ 
double uniform_likelihood(double*, int, double, double);
double gaussian_likelihood(double*, int, double, double);
double beta_likelihood(double*, int, double, double);
double gamma_likelihood(double*, int, double, double);
double binomial_likelihood(int *, int, int, double);
double bernoulli_likelihood(int*, int, double);

#endif
