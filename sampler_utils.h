#ifndef _SAMPLER_UTILS_H_
#define _SAMPLER_UTILS_H_

/* A stock callback */
void print_sample(double*, int);

/* A simple CSV reader */
void read_file(char*, double**, int*);

#endif
