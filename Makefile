CC=nvcc.exe
OPTS=-O2 -Wno-deprecated-gpu-targets

all: densities.o sampler.o sampler_utils.o infer_gaussian coinmint

sampler.o: sampler.h sampler.c
	$(CC) $(OPTS) -x cu -dc -o sampler.o sampler.c
	
sampler_utils.o: sampler_utils.h sampler_utils.c
	$(CC) $(OPTS) -x cu -dc -o sampler_utils.o sampler_utils.c
	
densities.o: densities.h densities.c
	$(CC) $(OPTS) -x cu -dc -o densities.o densities.c
	
infer_gaussian: densities.o sampler.o sampler_utils.o infer_gaussian.c
	$(CC) $(OPTS) -x cu -dc -o infer_gaussian.o infer_gaussian.c
	$(CC) $(OPTS) -o infer_gaussian infer_gaussian.o sampler.o sampler_utils.o densities.o

coinmint: densities.o sampler.o sampler_utils.o coinmint.c
	$(CC) $(OPTS) -x cu -dc -o coinmint.o coinmint.c
	$(CC) $(OPTS) -o coinmint coinmint.o sampler.o sampler_utils.o densities.o

coinmint_gpu: densities.o sampler.o sampler_utils.o coinmint_gpu.cu
	$(CC) $(OPTS) -dc -o coinmint_gpu.o coinmint_gpu.cu
	$(CC) $(OPTS) -o coinmint_gpu coinmint_gpu.o sampler.o sampler_utils.o densities.o	
	
lightning_gpu: densities.o sampler.o sampler_utils.o lightning_gpu.cu
	$(CC) $(OPTS) -dc -o lightning_gpu.o lightning_gpu.cu
	$(CC) $(OPTS) -o lightning_gpu lightning_gpu.o sampler.o sampler_utils.o densities.o		
	
clean:
	rm -f *.o *.bak infer_gaussian coinmint coinmint_gpu *.exe *.exp *.lib

