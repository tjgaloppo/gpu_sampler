#define _USE_MATH_DEFINES
#include <math.h>
#include "densities.h"

#define M_LN_PI 1.144729885849400174143 
#define M_LN_ROOT_2PI 0.918938533204673 // ln(sqrt(2*pi))

__host__ __device__
static double ln_nCk(double n, double k) {
  double d = n - k;
  return (n + 0.5) * log(n) -
         (k + 0.5) * log(k) -
         (d + 0.5) * log(d) -
         M_LN_ROOT_2PI;
}

__host__ __device__
double duniform(double x, double a, double b) {
	if (x >= a && x <= b)
  	return -log(b-a);
  else
	  return -INFINITY;
}

double uniform_likelihood(double* x, int n, double a, double b) {
  double sum = 0.0;
  double log_delta = -log(b-a);
  for (int j=0; j<n; j++) {
  	sum += (x[j] >= a && x[j] <= b) ? log_delta : -INFINITY;
  }
	return sum;
}

__host__ __device__
double dgaussian(double x, double mu, double sigma) {
  double delta = (x - mu);
  double ratio = delta / sigma;
  return -M_LN_ROOT_2PI - log(sigma) - 0.5 * ratio * ratio;
}

double gaussian_likelihood(double* x, int n, double mu, double sigma) {
  double sum = 0.0;
  double u = -M_LN_ROOT_2PI - log(sigma);
  for (int j=0; j<n; j++) {
  	double delta = x[j] - mu;
    double ratio = delta / sigma;
    sum += u - 0.5 * ratio * ratio;
  }
  return sum;
}

__host__ __device__
double dbeta(double x, double alpha, double beta) {
  return (alpha-1) * log(x) + (beta-1) * log(1.0-x) + lgamma(alpha+beta) - lgamma(alpha) - lgamma(beta);
}

double beta_likelihood(double* x, int n, double alpha, double beta) {
  double sum = 0.0;
  double u = lgamma(alpha+beta) - lgamma(alpha) - lgamma(beta);
  for (int j=0; j<n; j++) {
  	sum += (alpha-1) * log(x[j]) + (beta-1) * log(1.0-x[j]) + u;
  }
  return sum;
}

__host__ __device__
double dgamma(double x, double alpha, double beta) {
	return alpha * log(beta) - lgamma(alpha) + (alpha-1) * log(x) - beta * x;
}

double gamma_likelihood(double* x, int n, double alpha, double beta) {
  double w = alpha * log(beta) - lgamma(alpha);
  double sum = n * w;
  for (int j=0; j<n; j++) {
  	sum += (alpha-1.0) * log(x[j]) - beta * x[j];	
  }
  return sum;
}

__host__ __device__
double dbinomial(int k, int n, double p) {
  return ln_nCk(n,k) + k * log(p) + (n-k) * log(1.0-p);
}

double binomial_likelihood(int* k, int N, int n, double p) {
  double log_p = log(p);
  double log_1mp = log(1.0-p);
  double sum = 0.0;
  for (int j=0; j<N; j++) {
  	sum += ln_nCk(n, k[j]) + k[j] * log_p + (n-k[j]) * log_1mp;	
  }
  return sum;
}

__host__ __device__
double dbernoulli(int k, double p) {
	return k == 1 ? log(p) : log(1.0-p);
}

double bernoulli_likelihood(int* k, int n, double p) {
  double logp[] = { log(1.0-p), log(p) };
  double sum = 0.0;
 	for (int j=0; j<n; j++) {
 		sum += logp[k[j]];
 	} 
  return sum;
}
