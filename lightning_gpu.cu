/**

Mixed Effects Logistic Regression

* Single observed variable (strikes)
* Failures observed for 29 models
* Random intercepts + slopes

Let:
	x.1[] is strike count
	x.2[] is model index (1..29)
	y[] is non-fail/fail (0/1)

LME4 model: glmer(y ~ x.1 + (x.1 | x.2), family=binomial)
JAGS model: 

model {
	for (j in 1:N) {
		u <- beta.0 + beta.1 * x.1[j] + alpha.0[x.2[j]] + alpha.1[x.2[j]] * x.1[j]
		p <- 1.0 / (1.0 + exp(-u))
		y[j] ~ dbern(p)
	}
	
	for (j in 1:29) {
		alpha.0[j] ~ dnorm(0, 1/sigma^2)
		alpha.1[j] ~ dnorm(0, 1/tau^2)
	}
	
	beta.0 ~ dnorm(0, 1/100)
	beta.1 ~ dnorm(0, 1/100)
	sigma ~ dunif(0.001, 10)
	tau ~ dunif(0.001, 10)
}

**/

#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#define _USE_MATH_DEFINES

#include <windows.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#include "densities.h"
#include "sampler.h"
#include "sampler_utils.h"

double* host_data = NULL;
double* dev_data = NULL;
double* host_result = NULL;
double* dev_result = NULL;
double* host_x = NULL;
double* dev_x = NULL;
int ndata = 0;
int nblock = 0;
int nthread = 1024;
int shm_sz = 0;

int* model_idx;

__host__ __device__ double g(double* x, int m_id, double strikes) {
	return x[0] + x[2+m_id] + (x[1] + x[32+m_id]) * strikes;
}

__host__ __device__ double logit(double x) {
	return 1.0 / (1.0 + exp(-x));
}

__device__ void sum_array(double* x, int n) {
	int tid = threadIdx.x;
	do {
		n /= 2;
		if (tid < n) {
			x[tid] += x[tid + n];
		}
		__syncthreads();
	} while (n > 1);
}

__global__ void sum_vector(double* vec, int n) {
	extern __shared__ double cache[];
	
	int tid = threadIdx.x;
	cache[tid] = 0.0;
	int idx = tid;
	while (idx < n) {
		cache[tid] += vec[idx];
		idx += blockDim.x;
	}
	__syncthreads();
	sum_array(cache, blockDim.x);
	if (tid == 0) {
		vec[0] = cache[0];
	}
}

__global__ void compute_likelihood(double* result, double* data, int offset, int m, double* dev_x) {
	extern __shared__ double cache[];

	int bid = blockIdx.x;
	int tid = threadIdx.x;
	int uidx = bid * blockDim.x + tid;

	cache[tid] = 0.0;
	if (uidx < m) {
		int index = 3 * (offset + uidx);
		int model = data[index] - 1;		
		int failed = data[index+2];
		double strikes = data[index+1];
		double p = logit( g(dev_x, model, strikes) );
		cache[tid] = dbernoulli(failed, p);
	}

	__syncthreads();

	sum_array(cache, blockDim.x);

	if (tid == 0) {
		result[bid] = cache[0];
	}
}

__global__ void compute_likelihood_meta(double* u, int n, double sd) {
	extern __shared__ double cache[];
	int tid = threadIdx.x;
	cache[tid] = 0.0;
	if (tid < n) {
		cache[tid] = dgaussian(u[tid], 0, sd);
	}
	__syncthreads();
	sum_array(cache, blockDim.x);
	if (tid == 0) {
		u[0] = cache[0];
	}
}

double data_likelihood_cpu(double* x, int id) {
	double sum = dgaussian(x[id], 0, 10);
	for (int j = 0; j<ndata; j++) {
		int model = host_data[j*3] - 1;
		int fail = host_data[j*3+2];
		double strikes = host_data[j*3+1];
		double p = logit( g(x, model, strikes) );
		sum += dbernoulli(fail, p);
	}
	return sum;
}

double alpha_likelihood_cpu(double* x, int id) {
	double sum = dgaussian(x[2+id], 0, x[31]);
	for (int j = model_idx[id]; j < model_idx[id+1]; j++) {
		int fail = host_data[j*3+2];
		double strikes = host_data[j*3+1];
		double p = logit( g(x, id, strikes) );
		sum += dbernoulli(fail, p);
	}
	return sum;
}

double gamma_likelihood_cpu(double* x, int id) {
	double sum = dgaussian(x[32+id], 0, x[61]);
	for (int j=model_idx[id]; j < model_idx[id+1]; j++) {
		int fail = host_data[j*3+2];
		double strikes = host_data[j*3+1];
		double p = logit( g(x, id, strikes) );
		sum += dbernoulli(fail, p);		
	}
	return sum;
}

double sigma_likelihood_cpu(double* x) {
	double sum = duniform(x[31], 0.001, 10);
	for (int j=0; j<29; j++) {
		sum += dgaussian(x[2+j], 0, x[31]);
	}
	return sum;
}

double tau_likelihood_cpu(double* x) {
	double sum = duniform(x[61], 0.001, 10);
	for (int j=0; j<29; j++) {
		sum += dgaussian(x[32+j], 0, x[61]);
	}
	return sum;
}

double sigma_likelihood_gpu(double* x) {
	cudaMemcpy(dev_result, &x[2], sizeof(double)*29, cudaMemcpyHostToDevice);
	compute_likelihood_meta<<<1, 32, sizeof(double)*32>>>(dev_result, 29, x[31]);
	double sum = duniform(x[31], 0.001, 10);
	cudaDeviceSynchronize();
	sum += host_result[0];
//	double hresult = sigma_likelihood_cpu(x);
//	printf("check(sigma): %.6f %.6f\n", sum, hresult);	
	return sum;
}

double tau_likelihood_gpu(double* x) {
	cudaMemcpy(dev_result, &x[32], sizeof(double)*29, cudaMemcpyHostToDevice);
	compute_likelihood_meta<<<1, 32, sizeof(double)*32>>>(dev_result, 29, x[61]);
	double sum = duniform(x[61], 0.001, 10);
	cudaDeviceSynchronize();
	sum += host_result[0];
//	double hresult = tau_likelihood_cpu(x);
//	printf("check(tau): %.6f %.6f\n", sum, hresult);	
	return sum;
}

double data_likelihood_gpu(double* x, int id) {
	cudaMemcpy(dev_x, host_x, sizeof(double)*62, cudaMemcpyHostToDevice);
	compute_likelihood<<<nblock, nthread, shm_sz>>>(dev_result, dev_data, 0, ndata, dev_x);
	sum_vector<<<1,nthread,shm_sz>>>(dev_result, nblock);
	double result = dgaussian(x[id], 0, 10);
	cudaDeviceSynchronize();
	result += host_result[0];
//	double hresult = data_likelihood_cpu(x, id);
//	printf("check(beta): %.6f %.6f\n", result, hresult);
	return result;
}

double alpha_likelihood_gpu(double* x, int id) {
	cudaMemcpy(dev_x, host_x, sizeof(double)*62, cudaMemcpyHostToDevice);
	int ndat = model_idx[id+1] - model_idx[id];
	int nbloc = (ndat + nthread - 1) / nthread;
	compute_likelihood<<<nbloc, nthread, shm_sz>>>(dev_result, dev_data, model_idx[id], ndat, dev_x);
	sum_vector<<<1, nthread, shm_sz>>>(dev_result, nbloc);
	double result = dgaussian(x[2+id], 0, x[31]);
	cudaDeviceSynchronize();
	result += host_result[0];
//	double hresult = alpha_likelihood_cpu(x, id);
//  printf("check(alpha[%d]): %.6f %.6f\n", id, result, hresult);		
	return result;
}

double gamma_likelihood_gpu(double* x, int id) {
	cudaMemcpy(dev_x, host_x, sizeof(double)*62, cudaMemcpyHostToDevice);
	int ndat = model_idx[id+1] - model_idx[id];
	int nbloc = (ndat + nthread - 1) / nthread;
	compute_likelihood<<<nbloc, nthread, shm_sz>>>(dev_result, dev_data, model_idx[id], ndat, dev_x);
	sum_vector<<<1, nthread, shm_sz>>>(dev_result, nbloc);
	double result = dgaussian(x[32+id], 0, x[61]);
	cudaDeviceSynchronize();
	result += host_result[0];
//	double hresult = gamma_likelihood_cpu(x, id);
//  printf("check(gamma[%d]): %.6f %.6f\n", id, result, hresult);		
	return result;	
}

double (*data_likelihood)(double*, int) = data_likelihood_gpu;
double (*alpha_likelihood)(double*, int) = alpha_likelihood_gpu;
double (*sigma_likelihood)(double*) = sigma_likelihood_gpu;
double (*gamma_likelihood_f)(double*, int) = gamma_likelihood_gpu;
double (*tau_likelihood)(double*) = tau_likelihood_gpu;

// meta-parameter for prior distribution of random intercepts, alpha ~ N(0, sigma)
double sigma(double* x) {	return sigma_likelihood(x); }

// meta-parameter for prior distribution of random slopes, gamma ~ N(0, gamma)
double tau(double* x) {	return tau_likelihood(x); }

// fixed effect parameters (0=intercept, 1=slope)
double beta0(double* x) {	return data_likelihood(x, 0); }
double beta1(double* x) {	return data_likelihood(x, 1); }

// random effect intercepts
double alpha0(double* x) { return alpha_likelihood(x, 0); }
double alpha1(double* x) { return alpha_likelihood(x, 1); }
double alpha2(double* x) { return alpha_likelihood(x, 2); }
double alpha3(double* x) { return alpha_likelihood(x, 3); }
double alpha4(double* x) { return alpha_likelihood(x, 4); }
double alpha5(double* x) { return alpha_likelihood(x, 5); }
double alpha6(double* x) { return alpha_likelihood(x, 6); }
double alpha7(double* x) { return alpha_likelihood(x, 7); }
double alpha8(double* x) { return alpha_likelihood(x, 8); }
double alpha9(double* x) { return alpha_likelihood(x, 9); }
double alpha10(double* x) {	return alpha_likelihood(x, 10); }
double alpha11(double* x) {	return alpha_likelihood(x, 11); }
double alpha12(double* x) {	return alpha_likelihood(x, 12); }
double alpha13(double* x) {	return alpha_likelihood(x, 13); }
double alpha14(double* x) {	return alpha_likelihood(x, 14); }
double alpha15(double* x) {	return alpha_likelihood(x, 15); }
double alpha16(double* x) {	return alpha_likelihood(x, 16); }
double alpha17(double* x) {	return alpha_likelihood(x, 17); }
double alpha18(double* x) {	return alpha_likelihood(x, 18); }
double alpha19(double* x) {	return alpha_likelihood(x, 19); }
double alpha20(double* x) {	return alpha_likelihood(x, 20); }
double alpha21(double* x) {	return alpha_likelihood(x, 21); }
double alpha22(double* x) {	return alpha_likelihood(x, 22); }
double alpha23(double* x) {	return alpha_likelihood(x, 23); }
double alpha24(double* x) {	return alpha_likelihood(x, 24); }
double alpha25(double* x) {	return alpha_likelihood(x, 25); }
double alpha26(double* x) {	return alpha_likelihood(x, 26); }
double alpha27(double* x) {	return alpha_likelihood(x, 27); }
double alpha28(double* x) {	return alpha_likelihood(x, 28); }

// random effect slopes
double gamma0(double* x) { return gamma_likelihood_f(x, 0); }
double gamma1(double* x) { return gamma_likelihood_f(x, 1); }
double gamma2(double* x) { return gamma_likelihood_f(x, 2); }
double gamma3(double* x) { return gamma_likelihood_f(x, 3); }
double gamma4(double* x) { return gamma_likelihood_f(x, 4); }
double gamma5(double* x) { return gamma_likelihood_f(x, 5); }
double gamma6(double* x) { return gamma_likelihood_f(x, 6); }
double gamma7(double* x) { return gamma_likelihood_f(x, 7); }
double gamma8(double* x) { return gamma_likelihood_f(x, 8); }
double gamma9(double* x) { return gamma_likelihood_f(x, 9); }
double gamma10(double* x) { return gamma_likelihood_f(x, 10); }
double gamma11(double* x) {	return gamma_likelihood_f(x, 11); }
double gamma12(double* x) {	return gamma_likelihood_f(x, 12); }
double gamma13(double* x) {	return gamma_likelihood_f(x, 13); }
double gamma14(double* x) {	return gamma_likelihood_f(x, 14); }
double gamma15(double* x) {	return gamma_likelihood_f(x, 15); }
double gamma16(double* x) {	return gamma_likelihood_f(x, 16); }
double gamma17(double* x) {	return gamma_likelihood_f(x, 17); }
double gamma18(double* x) {	return gamma_likelihood_f(x, 18); }
double gamma19(double* x) {	return gamma_likelihood_f(x, 19); }
double gamma20(double* x) {	return gamma_likelihood_f(x, 20); }
double gamma21(double* x) {	return gamma_likelihood_f(x, 21); }
double gamma22(double* x) {	return gamma_likelihood_f(x, 22); }
double gamma23(double* x) {	return gamma_likelihood_f(x, 23); }
double gamma24(double* x) {	return gamma_likelihood_f(x, 24); }
double gamma25(double* x) {	return gamma_likelihood_f(x, 25); }
double gamma26(double* x) {	return gamma_likelihood_f(x, 26); }
double gamma27(double* x) {	return gamma_likelihood_f(x, 27); }
double gamma28(double* x) {	return gamma_likelihood_f(x, 28); }

void print_sample_tj(double* x, int k) {
		printf("%f , %f , %f , %f\n", x[0], x[1], x[31], x[61]);
}

int cmpRec(const void *a, const void* b) {
	if ( *(double*)a < *(double*)b ) return -1;
	if ( *(double*)a == *(double*)b ) return 0;
	return 1;	 
}

int main(int argc, char* argv[]) {
	srand((unsigned int)time(NULL));

	if (argc < 3) {
		printf("USAGE: %s <input file> <nsamples>\n", argv[0]);
		return 0;
	}

	if (argc == 4 && !strcmp(argv[3], "cpu")) {
		data_likelihood = data_likelihood_cpu;
		alpha_likelihood = alpha_likelihood_cpu;
		sigma_likelihood = sigma_likelihood_cpu;
		gamma_likelihood_f = gamma_likelihood_cpu;
		tau_likelihood = tau_likelihood_cpu;
	}

	read_file(argv[1], &host_data, &ndata);
	int nsample = atoi(argv[2]);

	qsort(host_data, ndata, sizeof(double)*3, cmpRec);
	model_idx = (int*)malloc(sizeof(int) * 30);
	model_idx[0]  = 0;
	model_idx[29] = ndata;

	double id = host_data[0];
	int index = 0;
	for (int j=1; j<ndata; j++) {
		if (id != host_data[j*3]) {
			id = host_data[j*3];
			model_idx[++index] = j;
		}
	}
	
	nblock = (ndata + nthread - 1) / nthread;
	shm_sz = sizeof(double) * nthread;

	cudaSetDevice(0);

	cudaMalloc(&dev_data, sizeof(double) * 3 * ndata);
	cudaMemcpy(dev_data, host_data, sizeof(double) * 3 * ndata, cudaMemcpyHostToDevice);

	cudaHostAlloc(&host_result, sizeof(double) * nblock, cudaHostAllocMapped);
	cudaHostGetDevicePointer(&dev_result, host_result, 0);

	density f[] = {
		beta0, beta1,
		alpha0, alpha1, alpha2, alpha3, alpha4, alpha5, alpha6,
		alpha7, alpha8, alpha9, alpha10, alpha11, alpha12, alpha13,
		alpha14, alpha15, alpha16, alpha17, alpha18, alpha19, alpha20,
		alpha21, alpha22, alpha23, alpha24, alpha25, alpha26, alpha27, alpha28,
		sigma,
		gamma0, gamma1, gamma2, gamma3, gamma4, gamma5, gamma6, gamma7, gamma8,
		gamma9, gamma10, gamma11, gamma12, gamma13, gamma14, gamma15, gamma16, gamma17,
		gamma18, gamma19, gamma20, gamma21, gamma22, gamma23, gamma24, gamma25, gamma26,
		gamma27, gamma28,
		tau
	};

	host_x = (double*)malloc(sizeof(double) * 62);
	cudaMalloc(&dev_x, sizeof(double)*62);

	for (int j = 0; j<62; j++) {
		host_x[j] = rand() / (double)RAND_MAX;
	}

	sampler(nsample, f, host_x, 62, print_sample);

	cudaFree(dev_x);
	cudaFree(dev_data);
  cudaFreeHost(host_result);	
	cudaDeviceReset();

	free(host_x);
	free(model_idx);
	free(host_data);

	return 0;
}
