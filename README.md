## About ##

This is is a proof-of-concept project for accelerated Gibbs sampling using GPU (CUDA) computing.  Specifically, the data likelihood component of the conditional posterior distributions is offloaded to the GPU.

## Building ##

The makefile serves as a template for compiling test instances.  My development environment is Windows based, so some massaging of the makefile for UNIX / Linux environments may be necessary.

## How to use ##

I will provide some instructions for building test instances soon.

### Creator / Maintainer ###

* Travis Galoppo (tjg2107 AT columbia.edu)
